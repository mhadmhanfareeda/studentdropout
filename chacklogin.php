<?php 
    session_start();
    include "configs/db.php";
    $conn = OpenCon();
    $errors = array();
    $allowAccess = array('0', '1', '3', '4');
    $allowNotAccess = array('5', '6', '7');

    if (isset($_POST['login_user'])) {
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);

        if (empty($username)) {
            array_push($errors, "Username is required");
        }

        if (empty($password)) {
            array_push($errors, "Password is required");
        }

        if (count($errors) == 0) {
            $query = "SELECT * FROM account WHERE username = '$username' AND password = '$password' ";
            $result = $conn->query($query);
            $row = $result->fetch_assoc();

            if ($result->num_rows >0) {
                if(in_array($row["level_id"], $allowAccess )){
                    $_SESSION['username'] = $username;
                    $_SESSION['leveluid'] = $row["level_id"];
                    $_SESSION['provinceuid'] = $row["province_id"];
                    echo "S";
                }else{
                    echo "LW";
                }
            } else {
                echo "W";
            }
        } else {
            echo "Username & Password is required";
        }
    }
