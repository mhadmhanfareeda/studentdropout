<link rel="stylesheet" type="text/css" href="../../views/editdetailsstudent/editdetailsstudent.css" />
<?php
include "../../models/user.php";
include "../../models/querycommon.php";
$resultPro = queryprovince();
$resultstatus = querystatusdrop();
$resultjurisdiction = queryjurisdiction();
$studentuid = $_POST["studentuid"];
$jurisdictionuid = @$_POST['jurisdictionuid'];
$obj = new User;
$data = $obj->queryUserdetails($studentuid);
$dataStudent = json_decode($data);
?>
<div>

    <?php
    if ($dataStudent->{'statusDrop_id'} != '009') { ?>
        <h5 align="center"><img style="text-align: center;width:60px;" src="../../public/fileupload/student/<?= $studentuid ?>.png" /></h5>
    <?php
    }
    ?>

    <h4 style="text-align: center;"><b>บันทึกข้อมูลการติดตาม</b></h4>
</div>
<div class="container">
    <div class="maineditdata">
        <!-- ข้อมูลบุคคล -->
        <div class="row">
            <hr>
            <h4><b>ข้อมูลบุคคล</b></h4>
            <div class="columneditdata">
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> วันที่เพิ่มข้อมูล : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'created'}) {
                                                                                                echo $dataStudent->{'created'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> วันที่อัปเดตข้อมูล : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'updated'}) {
                                                                                                echo $dataStudent->{'updated'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสประจำตัวประชาชน : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?= $dataStudent->{'personuid'} ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> เลขที่หนังสือเดินทาง : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'passportnumber'}) {
                                                                                                echo $dataStudent->{'passportnumber'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสนักเรียน : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?= $studentuid ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสคำนำหน้าชื่อ : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'prefixName'}) {
                                                                                                echo $dataStudent->{'prefix_id'}." - ".$dataStudent->{'prefixName'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ชื่อ-นามสกุล : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?= $dataStudent->{'firstname'} ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ชื่อกลาง : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'middlename'}) {
                                                                                                echo $dataStudent->{'middlename'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ปีการศึกษา : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'academicyear'}) {
                                                                                                echo $dataStudent->{'academicyear'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ภาคการศึกษา : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'semester'}) {
                                                                                                echo $dataStudent->{'semester'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> วันเดือนปีเกิด : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'birthdate'}) {
                                                                                                echo $dataStudent->{'birthdate'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสหน่วยงานสำนักงาน กศน. : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'organize_id'}) {
                                                                                                echo $dataStudent->{'organize_id'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสสถานศึกษา/หน่วยงาน : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'school_id'}) {
                                                                                                echo $dataStudent->{'school_id'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> เพศ : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'gender_id'} == '1') {
                                                                                                echo "1 - เพศชาย";
                                                                                            } else if ($dataStudent->{'gender_id'} == '2') {
                                                                                                echo "2 - เพศหญิง";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> สัญชาติ : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'nationality_id'}) {
                                                                                                echo $dataStudent->{'nationality_id'} . " - " . $dataStudent->{'nationality_name'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสความพิการ : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'disability_id'}) {
                                                                                                echo $dataStudent->{'disability_id'} . " - " . $dataStudent->{'disability_name'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสความด้อยโอกาส : </div>
                            <div class="col-sm-8" align="left">
                                <input disabled name="Admin_username" type="text" value="<?php if ($dataStudent->{'disadvantageeducation_id'}) {
                                                                                                echo $dataStudent->{'disadvantageeducation_id'} . " - " . $dataStudent->{'disadvantageeducation_name'};
                                                                                            } else {
                                                                                                echo "-";
                                                                                            } ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ข้อมูลที่อยู่ (ข้อมูลที่อยู่ปัจุบัน) -->
        <div class="row">
            <hr>
            <h4><b>ข้อมูลที่อยู่ปัจุบัน</b></h4>
            <div class="columneditdata">
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสประจำบ้าน :</div>
                            <div class="col-sm-8" align="left">
                                <input name="houseid_current" id="houseid_current" type="text" value="<?php if ($dataStudent->{'houseid'}) {
                                                                                echo $dataStudent->{'houseid'};
                                                                            } ?>" class="form-control" placeholder="รหัสประจำบ้าน" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * เลขที่บ้าน : </div>
                            <div class="col-sm-8" align="left">
                                <input name="house_current" type="text" value="<?php if ($dataStudent->{'house'}) {
                                                                            echo $dataStudent->{'house'};
                                                                        } ?>" class="form-control" id="house_current" placeholder="เลขที่บ้าน" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> หมู่บ้าน/คอนโด/อพาร์ทเม้นท์ : </div>
                            <div class="col-sm-8" align="left">
                                <input name="condo_current" type="text" value="<?php if ($dataStudent->{'condoCurrent'}) {
                                                                            echo $dataStudent->{'condoCurrent'};
                                                                        }  ?>" class="form-control" id="condo_current" placeholder="หมู่บ้าน/คอนโด/อพาร์ทเม้นท์" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * หมู่ที่ : </div>
                            <div class="col-sm-8" align="left">
                                <input name="villagenumber_current" type="text" value="<?php if ($dataStudent->{'villagenumber'}) {
                                                                                    echo $dataStudent->{'villagenumber'};
                                                                                }  ?>" class="form-control" id="villagenumber_current" placeholder="หมู่ที่" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ซอย : </div>
                            <div class="col-sm-8" align="left">
                                <input name="soi_current" type="text" value="<?php if ($dataStudent->{'soi'}) {
                                                                            echo $dataStudent->{'soi'};
                                                                        }  ?>" class="form-control" id="soi_current" placeholder="ซอย" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ถนน : </div>
                            <div class="col-sm-8" align="left">
                                <input name="street_current" type="text" value="<?php if ($dataStudent->{'street'}) {
                                                                            echo $dataStudent->{'street'};
                                                                        }  ?>" class="form-control" id="street_current" placeholder="ถนน" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-12">
                    <form name="pic" id="pic" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * จังหวัด : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="provinceuid_current" id="provinceuid_current" onchange="addressSel('province', 'current')">
                                    <option value="">กรุณาเลือกจังหวัด</option>
                                    <?php
                                    foreach ($resultPro as $rowPro) {
                                    ?>
                                        <option value="<?= $rowPro["province_id"] ?>" <?php if ($rowPro["province_name"] == $dataStudent->{'provinceCurrent'}) {
                                                                                            echo 'selected';
                                                                                        } ?>><?= $rowPro["province_name"] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * อำเภอ : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="district_current" id="district_current" onchange="addressSel('district', 'current')">
                                    <option value="<?= $dataStudent->{'districtuidCurrent'} ?>" selected><?= $dataStudent->{'districtCurrent'} ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * ตำบล : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="subdistrict_current" id="subdistrict_current">
                                    <option value="<?= $dataStudent->{'subdistrictuidCurrent'} ?>" selected><?= $dataStudent->{'subdistrictCurrent'} ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * ละติจูด : </div>
                            <div class="col-sm-8" align="left">
                                <input name="latitude_current" type="text" value="<?php if ($dataStudent->{'latitude'}) {
                                                                                echo $dataStudent->{'latitude'};
                                                                            } ?>" class="form-control" id="latitude_current" placeholder="กรุณาใส่ละจิจูด" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" maxlength="10" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * ลองจิจูด : </div>
                            <div class="col-sm-8" align="left">
                                <input name="longitude_current" type="text" value="<?php if ($dataStudent->{'longitude'}) {
                                                                                echo $dataStudent->{'longitude'};
                                                                            } ?>" class="form-control" id="longitude_current" placeholder="กรุณาใส่ลองจิจูด" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" maxlength="10" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ข้อมูลที่อยู่ (ข้อมูลที่อยู่ตามทะเบียนบ้าน) -->
        <div class="row">
            <hr>
            <h4><b>ข้อมูลที่อยู่ตามทะเบียนบ้าน</b></h4>
            <div class="columneditdata">
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> รหัสประจำบ้าน :</div>
                            <div class="col-sm-8" align="left">
                                <input name="houseid" type="text" value="<?php if ($dataStudent->{'houseid'}) {
                                                                                echo $dataStudent->{'houseid'};
                                                                            } ?>" class="form-control" id="houseid" placeholder="รหัสประจำบ้าน" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * เลขที่บ้าน : </div>
                            <div class="col-sm-8" align="left">
                                <input name="house" type="text" value="<?php if ($dataStudent->{'house'}) {
                                                                            echo $dataStudent->{'house'};
                                                                        } ?>" class="form-control" id="house" placeholder="เลขที่บ้าน" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> หมู่บ้าน/คอนโด/อพาร์ทเม้นท์ : </div>
                            <div class="col-sm-8" align="left">
                                <input name="condo" type="text" value="<?php if ($dataStudent->{'condo'}) {
                                                                            echo $dataStudent->{'condo'};
                                                                        }  ?>" class="form-control" id="condo" placeholder="หมู่บ้าน/คอนโด/อพาร์ทเม้นท์" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * หมู่ที่ : </div>
                            <div class="col-sm-8" align="left">
                                <input name="villagenumber" type="text" value="<?php if ($dataStudent->{'villagenumber'}) {
                                                                                    echo $dataStudent->{'villagenumber'};
                                                                                }  ?>" class="form-control" id="villagenumber" placeholder="หมู่ที่" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ซอย : </div>
                            <div class="col-sm-8" align="left">
                                <input name="soi" type="text" value="<?php if ($dataStudent->{'soi'}) {
                                                                            echo $dataStudent->{'soi'};
                                                                        }  ?>" class="form-control" id="soi" placeholder="ซอย" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> ถนน : </div>
                            <div class="col-sm-8" align="left">
                                <input name="street" type="text" value="<?php if ($dataStudent->{'street'}) {
                                                                            echo $dataStudent->{'street'};
                                                                        }  ?>" class="form-control" id="street" placeholder="ถนน" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-12">
                    <form name="pic" id="pic" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * จังหวัด : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="provinceuid" id="provinceuid" onchange="addressSel('province', 'home')">
                                    <option value="">กรุณาเลือกจังหวัด</option>
                                    <?php
                                    foreach ($resultPro as $rowPro) {
                                    ?>
                                        <option value="<?= $rowPro["province_id"] ?>" <?php if ($rowPro["province_name"] == $dataStudent->{'province'}) {
                                                                                            echo 'selected';
                                                                                        } ?>><?= $rowPro["province_name"] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * อำเภอ : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="district" id="district" onchange="addressSel('district', 'home')">
                                    <option value="<?= $dataStudent->{'district_id'} ?>" selected><?= $dataStudent->{'district'} ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * ตำบล : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="subdistrict" id="subdistrict">
                                    <option value="<?= $dataStudent->{'subdistrict_id'} ?>" selected><?= $dataStudent->{'subdistrict'} ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * ละติจูด : </div>
                            <div class="col-sm-8" align="left">
                                <input name="latitude" type="text" value="<?php if ($dataStudent->{'latitude'}) {
                                                                                echo $dataStudent->{'latitude'};
                                                                            } ?>" class="form-control" id="latitude" placeholder="กรุณาใส่ละจิจูด" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" maxlength="10" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * ลองจิจูด : </div>
                            <div class="col-sm-8" align="left">
                                <input name="longitude" type="text" value="<?php if ($dataStudent->{'longitude'}) {
                                                                                echo $dataStudent->{'longitude'};
                                                                            } ?>" class="form-control" id="longitude" placeholder="กรุณาใส่ลองจิจูด" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" maxlength="10"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ข้อมูลปรับสถานะภาพ -->
        <div class="row">
            <hr>
            <h4><b>ข้อมูลปรับสถานะภาพ</b></h4>
            <div class="columneditdata">
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * สาเหตุ : </div>
                            <div class="col-sm-8" align="left">
                                <select class="form-control" name="statusstudent_1" id="statusstudent_1" onchange="checkstatus()">
                                    <option value="">...Select...</option>
                                    <?php
                                    foreach ($resultstatus as $rowstatus) {
                                    ?>
                                        <option value="<?= $rowstatus["status_id"] ?>" <?php if ($rowstatus["status_id"] == $dataStudent->{'statusDrop_id'}) {
                                                                                            echo 'selected';
                                                                                        } ?>><?= $rowstatus["status_name"] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <!-- 001 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent001_2" id="statusstudent001_2">
                                    <option value="พบตัว">พบตัว</option>
                                </select>
                                <select style="display:none" class="form-control" name="statusstudent001_3" id="statusstudent001_3" onchange="checkstatus()">
                                    <option value="">...Select...</option>
                                    <option value="ศึกษาที่เดิม">ศึกษาที่เดิม</option>
                                    <option value="ศึกษาที่ใหม่">ศึกษาที่ใหม่</option>
                                    <option value="ศึกษาต่างประเทศ">ศึกษาต่างประเทศ</option>
                                </select>
                                <!-- 002 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent002_2" id="statusstudent002_2">
                                    <option value="พบตัว">พบตัว</option>
                                </select>
                                <select disabled style="display:none" class="form-control" name="statusstudent002_3" id="statusstudent002_3">
                                    <option value="ศึกษาที่เดิม">ศึกษาที่เดิม</option>
                                </select>
                                <!-- 003 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent003_2" id="statusstudent003_2">
                                    <option value="พบตัว">พบตัว</option>
                                </select>
                                <select disabled style="display:none" class="form-control" name="statusstudent003_3" id="statusstudent003_3">
                                    <option value="">-</option>
                                </select>
                                <!-- 004 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent004_2" id="statusstudent004_2">
                                    <option value="ไม่พบตัว">ไม่พบตัว</option>
                                </select>
                                <select disabled style="display:none" class="form-control" name="statusstudent004_3" id="statusstudent004_3">
                                    <option value="ย้ายที่อยู่">ย้ายที่อยู่</option>
                                </select>
                                <!-- 005 ลาออก-->
                                <select disabled style="display:none" class="form-control" name="statusstudent005_2" id="statusstudent005_2">
                                    <option value="พบตัว">พบตัว</option>
                                </select>
                                <input style="display:none" name="statusstudent005_3" id="statusstudent005_3" type="text" value="<?php if ($dataStudent->{'statusnote'}) {
                                                                                                                                        echo $dataStudent->{'statusnote'};
                                                                                                                                    } else {
                                                                                                                                    } ?>" class="form-control" placeholder="กรุณาใส่เหตุผลลาออก" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                                <!-- 006 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent006_2" id="statusstudent006_2">
                                    <option value="ไม่พบตัว">ไม่พบตัว</option>
                                </select>
                                <select disabled style="display:none" class="form-control" name="statusstudent006_3" id="statusstudent006_3">
                                    <option value="ติดต่อไม่ได้">ติดต่อไม่ได้</option>
                                </select>
                                <!-- 007 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent007_2" id="statusstudent007_2">
                                    <option value="พบตัว">พบตัว</option>
                                </select>
                                <select style="display:none" class="form-control" name="statusstudent007_3" id="statusstudent007_3">
                                    <option value="">...Select...</option>
                                    <option value="สำเร็จการศึกษาระดับประถมศึกษา">สำเร็จการศึกษาระดับประถมศึกษา</option>
                                    <option value="สำเร็จการศึกษาระดับมัธยมศึกษาตอนต้น(ภาคบังคับ)">สำเร็จการศึกษาระดับมัธยมศึกษาตอนต้น (ภาคบังคับ)</option>
                                    <option value="สำเร็จการศึกษาระดับมัธยมศึกษาตอนปลาย">สำเร็จการศึกษาระดับมัธยมศึกษาตอนปลาย</option>
                                </select>
                                <!-- 008 -->
                                <select disabled style="display:none" class="form-control" name="statusstudent008_2" id="statusstudent008_2">
                                    <option value="พบตัว">พบตัว</option>
                                </select>
                                <select style="display:none" class="form-control" name="statusstudent008_3" id="statusstudent008_3">
                                    <option value="">...Select...</option>
                                    <option value="ไม่สำเร็จการศึกษา-อายุพ้นเกณฑ์">ไม่สำเร็จการศึกษา-อายุพ้นเกณฑ์</option>
                                    <option value="ปัญหาด้านสุขภาพ/พิการ">ปัญหาด้านสุขภาพ/พิการ</option>
                                    <option value="สำเร็จการศึกษาระดับมัธยมศึกษาตอนต้น(ภาคบังคับ)">สำเร็จการศึกษาระดับมัธยมศึกษาตอนต้น(ภาคบังคับ)</option>
                                </select>
                                <!-- เลือกสังกัด -->
                                <select style="display:none" class="form-control" name="jurisdiction" id="jurisdiction" onchange="shJurisdiction(<?= $studentuid ?>)">
                                    <option value="">กรุณาเลือกสังกัด</option>
                                    <?php
                                    foreach ($resultjurisdiction as $rowjurisdiction) {
                                    ?>
                                        <option value="<?= $rowjurisdiction["jurisdiction_id"] ?>"><?= $rowjurisdiction["jurisdiction_name"] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <!-- เลือกโรงเรียนในสังกัด -->
                                <div id="shJurisdiction"></div>
                                <!-- ใส่ประเทศที่อยู่ -->
                                <div id="country" style="display:none">
                                    <input name="countrydetail" id="countrydetail" class="form-control" type="text" placeholder="ระบุประเทศ" title="ภาษาอังกฤษหรือภาษาไทยเท่านั้น" minlength="2" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ข้อมูลรูปภาพนักเรียน -->
        <div class="row">
            <hr>
            <h4><b>ข้อมูลรูปภาพนักเรียน *(png, jpeg, jpg)</b></h4>
            <div class="columneditdata">
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * รูปภาพนักเรียน : </div>
                            <div class="col-sm-8" align="left">
                                <input name="picstudent" type="file" class="form-control" id="picstudent" placeholder="กรุณาใส่ลองจิจูด" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" maxlength="11" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4" align="right"> * รูปภาพที่อยู่นักเรียน : </div>
                            <div class="col-sm-6" align="left">
                                <table id="dynamic_field">
                                    <tr>
                                        <td><input name="addpic[]" id='addpic_1' type="file" class="form-control" placeholder="กรุณาใส่ลองจิจูด" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" maxlength="11" /></td>
                                        <td><input type="button" value="+" onclick="addData()"></button></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-12">
                    <form name="register" id="register" class="form-horizontal">

                        <div class="form-group">
                            <div class="col-sm-4"> </div>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-primary" id="btn" onclick="savedetailsStudent(<?= $studentuid ?>)"> บันทึกข้อมูล </button>
                                <div id="statusupdate"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>