<?php session_start();
if (!isset($_SESSION['username'])) {
    header("location:../../index.php");
    exit();
}
include "../../configs/structureresoures.php";
include "../../configs/db.php";
$con = OpenCon();
$query = "SELECT organize_name, username FROM account WHERE username = '" . $_SESSION['username'] . "'";
$result =  $con->query($query);
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
}

?>
<div class="header">
    <div class="submenubar">
        <div>
            <img style="width:450" src="../../public/logonfe.png" />
        </div>
        <div class="container topbar">
            <div class="row">
                <div>
                    <div>
                        <p><b>รหัสผู้ใช้งาน :</b>&nbsp;&nbsp;<?= $row["username"] ?>&nbsp;&nbsp;&nbsp;<img class="img_container" src="../../public/user1.png" /></p>
                    </div>
                    <div>
                        <p><b>ชื่อหน่วยงาน/สถานศึกษา :</b>&nbsp;<?= $row["organize_name"] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main menubar">
    <div class="submenubar">
        <div>
            <ul name="choosepage" id="choosepage">
                <label>Menu</label>
                <li><a href="#main">หน้าหลัก</a></li>
                <!-- <li><a href="#report">รายงานนักเรียน</a></li> -->
                <li><a href="#change">เปลี่ยนรหัสผ่าน</a></li>
                <li><a href="#logout">Logout</a></li>
            </ul>
        </div>

        <div id="contentPage"></div>
    </div>
</div>


<script type="text/javascript" src="mainapp.js"></script>
