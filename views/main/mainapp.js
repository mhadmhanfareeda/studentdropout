$("#choosepage li").click(function() {
    $("#choosepage li").removeClass("active");
    $(this).addClass("active");

    var text = $(this).text();
    let data = {
        pagename: text
    };

    $.ajax({
        url: "mainapp.controller.php",
        method: "POST",
        data: data,
        success: function(response) {
            let data = response.split("/CUT/");
            $('#contentPage').html(data[1]);
            if (text == 'Logout') {
                window.location.replace("../../index.php");
            }else if(text == 'รายงานนักเรียน'){
                window.open("http://203.159.251.160/reportstudent/", "_blank");
            }
        }
    });

});

function nextpage(pagenumber) {
    let data = {
        page: pagenumber,
        level: $('#levelForLevel').val() || $('#level').val(),
        statusfind: $('#statusfindForLevel').val() || $('#statusfind').val(),
        usernameuid: $('#usernameuid').val() || $('#usernameuid').val(),
        statusstudent: $('#statusstudentForLevel').val() || $('#statusstudent').val(),
        yearofterm: $('#yearoftermForLevel').val() || $('#yearofterm').val(),
        term: $('#termForLevel').val() || $('#term').val(),
        schoolname: $('#organize').val() || $('#organizePro').val(),
        province: $('#province').val(),
        otherlevel: true
    }

    console.log(data);

    $.ajax({
        url: "../showalldetails/tablestudentalldetails.php",
        method: "POST",
        data: data,
        success: function (response) {
            $('#showDataStudent').html(response);
        }
    });
}

$(document).ready(function() {
    let data = {
        pagename: 'หน้าหลัก'
    };

    $.ajax({
        url: "mainapp.controller.php",
        method: "POST",
        data: data,
        success: function(response) {
            let data = response.split("/CUT/");
            $('#contentPage').html(data[1]);
        }
    });
})