
<?php
session_start();
include "../../configs/db.php";
$con = OpenCon();
$querysh = '';
$queryfirstpage = '';
$nameDatabase = '';
$pagesh = 0;
$perpage = 20;
$notFind = array('004', '006');
$find = array('001', '002', '003', '005', '007', '008');
$statusfind= "";
$statusstudent= "";
$level= "";
$term="";
$yearofterm="";
$schoolname="";
$province = @$_POST["province"]? @$_POST["province"]:$_SESSION['provinceuid'];
$statusfind = @$_POST["statusfind"];
$querysh='';
$sortOrder='';
$sortbyupdate = @$_POST["sortbyupdate"];

if (@$_POST["page"]) {
    $page = $_POST["page"];
    if ($page == '' || $page == '1') {
        $pagesh = 0;
    } else {
        $pagesh = ($page * $perpage) - $perpage;
    }
}

$oid = $pagesh +1 ;

if (@$_POST["yearofterm"] && @$_POST["term"]) {
    $yearofterm = $_POST["yearofterm"];
    $term = $_POST["term"];
    $nameDatabase = 'totalstudent' . $yearofterm . '_' . $term;
} else {
    $yearofterm = date("Y") + 543;
    $term = '1';
    $nameDatabase = 'totalstudent' . $yearofterm . '_1';
}

if($_SESSION['leveluid'] == '1' || $_SESSION['leveluid'] == '0' || $_SESSION['leveluid'] == '3'){
    if (@$_POST["statusstudent"] != 'All') {
        $statusstudent = @$_POST["statusstudent"];
        $querysh = " studentstaus_id = '{$statusstudent}'";
    }else{
        $querysh = " studentstaus_id IN ('001', '002', '003', '005', '007', '008', '004', '006', '009')";
        $statusstudent = @$_POST["statusstudent"];
    }

    if (@$_POST["schoolname"]) {
        $schoolname = $_POST["schoolname"];
        $querysh .= " AND school_id = '$schoolname'";
    }
}else{
    if ($_SESSION['username']) {
        $querysh = "school_id='" . $_SESSION['username'] . "'";
    }
    
    if (@$_POST["statusstudent"]) {
        $statusstudent = @$_POST["statusstudent"];
        $querysh .= " AND studentstaus_id = '{$statusstudent}'";
    }
}

if (@$_POST["statusfind"] == '1') {
    $querysh .= " AND studentstaus_id IN ('004', '006')";
} else if (@$_POST["statusfind"] == '2') {
    $querysh .= " AND studentstaus_id IN ('001', '002', '003', '005', '007', '008')";
}

if (@$_POST["level"]) {
    $level = $_POST["level"];
    $querysh .= " AND table1.educationlevel_id = '$level'";
}

if ($province) {
    $querysh .= " AND table1.province_id = '$province'";
}

$where = '';
if($querysh != ''){
$where = ' WHERE ';
}

if($sortbyupdate == 'false'){
    $sortOrder=" table1.firstname ASC ";
}else{
    $sortOrder=" table1.updated DESC ";
}
$sql = "SELECT table1.person_id,table1.school_id, table1.student_id, table1.firstname, table1.lastname, table1.studentstaus_id, table1.educationlevel_id, table2.organize_name FROM $nameDatabase table1, m_organize table2 $where table1.school_id = table2.organize_id AND $querysh ORDER BY $sortOrder LIMIT $pagesh, $perpage";
?>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    thead,
    td {
        text-align: left;
        padding: 8px;
    }

    thead {
        background: #2F4F4F;
        color: white;
    }
</style>
<div style="
    display: flex;
    width: 100%;
    justify-content: flex-end;
    padding-right: 2%;
    padding-bottom: 10px;
">
<a href="../../controllers/exportexcel.controller.php?level=<?=$level?>&statusstudent=<?=$statusstudent?>&statusfind=<?=$statusfind?>&yearofterm=<?=$yearofterm?>&term=<?=$term?>&province=<?=$province?>&schoolname=<?=$schoolname?>&sortbyupdate=<?=$sortbyupdate?>">  พิมพ์รายงาน <img src="../../public/excel.png" height="20" width="20"/></a></div>
<table>
    <thead>
        <td>ลำดับ</td>
        <td>เลขประจำตัวประชาชน</td>
        <td>เลขประจำตัวนักเรียน</td>
        <td>ชื่อ</td>
        <td>นามสกุล</td>
        <td>ระดับชั้นการศีกษา</td>
        <td>สถานะการติดตาม</td>
        <td>ปรับปรุง</td>
    </thead>
    <?php
    $result = $con->query($sql);
    if (@$result->num_rows > 0) {
        @$countrow = @$result->num_rows;
    } else {
        @$countrow = 0;
    }
    if (@$countrow > 0) {
        while ($row = $result->fetch_assoc()) {
            if ($row['educationlevel_id'] == '11') {
                $educationlevel = 'ประถม';
            } else if ($row['educationlevel_id'] == '12') {
                $educationlevel = 'มัธยมศึกษาตอนต้น';
            } else {
                $educationlevel = 'มัธยมศึกษาตอนปลาย';
            }

            if (in_array($row['studentstaus_id'], $notFind)) {
                $statusFind = 'ติดตามแล้วไม่พบตัว';
            } else if (in_array($row['studentstaus_id'], $find)) {
                $statusFind = 'ติดตามแล้วพบตัว';
            } else {
                $statusFind = 'หลุดจากระบบการศึกษา';
            }
            // statusDropname
            $statusDropname = '';
            $queryStatusDrop = "SELECT status_name FROM m_status_dropout WHERE status_id = '".$row['studentstaus_id']."'";
            $resultStatusDrop =  $con->query($queryStatusDrop);
            if ($resultStatusDrop->num_rows > 0) {
                $rowStatusDrop = $resultStatusDrop->fetch_assoc();
            }
            $statusDropname = $rowStatusDrop["status_name"];

    ?>
            <tbody style="background-color: <?php if (in_array($row['studentstaus_id'], $notFind)) {
                                                echo '#FA8072';
                                            } else if ($row['studentstaus_id'] == '009') {
                                                echo '#FFFF99';
                                            } else {
                                                echo '#33FF99';
                                            } ?>;">
                 <td><?= $oid ?></td>                            
                <td><?= $row['person_id'] ?></td>
                <td><?= $row['student_id'] ?></td>
                <td><?= $row['firstname'] ?></td>
                <td><?= $row['lastname'] ?></td>
                <td><?= $educationlevel ?></td>
                <td><?= $statusDropname ?></td>
                <td><img src="../../public/edit.png" width="40px" title="แก้ไขรายละเอียด" onclick="showdetailsStudent(<?= $row['student_id'] ?>)"></td>
            </tbody>
        <?php  $oid++;  } 
        } else {
        ?>
        <tbody>
            <tr>
                <td>ไม่มีข้อมูล</td>
            </tr>
        </tbody>
    <?php
    }
    ?>
</table>
<hr>
<div align= "right" >
<td>ผู้พัฒนาระบบ<br>
    <font face=angsananew color=660066 >กลุ่มเทคโนโลยีดิจิทัลและสารสนเทศ (กย.)</font><br>
    <font face=angsananew color=660066 >สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย </font><br>
    <font face=angsananew color=660066 >สำนักงานปลัดกระทรวงศึกษาธิการ </font>
</td></div></hr>

  