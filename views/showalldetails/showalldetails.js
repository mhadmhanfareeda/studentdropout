var countpage = 1;
var index = 1;

$(document).ready(function () {
    let data = {
        statusstudent: '009',
        fisrpage: true,
        sortbyupdate:false
    }

    $.ajax({
        url: "../../models/querynumberstudent.php",
        cache: false,
        processData: false,
        contentType: false,
        type: 'GET',
        success: function (response) {
            let arrayData = response.split("&");
            $('#tracktudentfind').html('<p class=number>' + arrayData[1] + '</p>');
            $('#tracktudentnotfind').html('<p class=number>' + arrayData[2] + '</p>');
            $('#dropstudent').html('<p class=number>' + arrayData[3] + '</p>');

            $('#sumboxfirst1').html('จำนวนนักเรียนทั้งหมด');
            $('#sumboxfirst2').html('จำนวนนักเรียนทั้งหมด');
            $('#sumboxfirst3').html('จำนวนนักเรียนทั้งหมด');
        }
    });

    $.ajax({
        url: "../../models/querynumberlevel.php",
        method: "POST",
        data: data,
        success: function (response) {
            let arrayData = response.split("&");
            $('#tracktudentfindOther').html('<p class=number>' + arrayData[1] + '</p>');
            $('#tracktudentnotfindOther').html('<p class=number>' + arrayData[2] + '</p>');
            $('#dropstudentOther').html('<p class=number>' + arrayData[3] + '</p>');

            $('#sumbox1').html('จำนวนนักเรียนในจังหวัด / โรงเรียน');
            $('#sumbox2').html('จำนวนนักเรียนในจังหวัด / โรงเรียน');
            $('#sumbox3').html('จำนวนนักเรียนในจังหวัด / โรงเรียน');
        }
    });

    $.ajax({
        url: "../showalldetails/tablestudentalldetails.php",
        method: "POST",
        data: data,
        beforeSend: function () {
            $("#loaderDiv").show();
        },
        success: function (response) {
            $("#loaderDiv").hide();
            $('#showDataStudent').html(response);
        }
    });

    $.ajax({
        url: "page.php",
        method: "POST",
        data: data,
        success: function (response) {
            localStorage.setItem('nameOfItem', response);
            let sumofpage = localStorage.getItem('nameOfItem');
            $('#allofpage').html(sumofpage);
            $('#allofpageschool').html(sumofpage);
        }
    });

});
function nexttable() {
    let level = $('#level').val() ? $('#level').val() : $('#levelForLevel').val();
    let statusfind = $('#statusfind').val() ? $('#statusfind').val() : $('#statusfindForLevel').val();
    let usernameuid = $('#usernameuid').val() ? $('#usernameuid').val() : $('#usernameuid').val();
    let statusstudent = $('#statusstudent').val() ? $('#statusstudent').val() : $('#statusstudentForLevel').val();
    let yearofterm = $('#yearofterm').val() ? $('#yearofterm').val() : $('#yearoftermForLevel').val();
    let term = $('#term').val() ? $('#term').val() : $('#termForLevel').val();
    let schoolname = $('#organizePro').val() ? $('#organizePro').val() : $('#organize').val();
    let province = $('#province').val();
    let sumofpage = localStorage.getItem('nameOfItem');
    let sortbyupdate = $("#sortbyupdate").is(":checked") ? "true" : "false";

    if (countpage == sumofpage || sumofpage == 0) {
        return false;
    }
    countpage++;
    $('#countsum').html(countpage);
    $('#countsumlevel').html(countpage);
    $('#allofpage').html(sumofpage);
    $('#allofpageschool').html(sumofpage);
    let data = {};
    data.level = level;
    data.statusfind = statusfind;
    data.statusstudent = statusstudent;
    data.yearofterm = yearofterm;
    data.term = term;
    data.page = countpage;
    data.sortbyupdate = sortbyupdate;

    if (schoolname && schoolname == '') {
        data.schoolname = schoolname;
    }

    if (province && province == '') {
        data.province = province;
    }

    console.log(data);

    $.ajax({
        url: "../showalldetails/tablestudentalldetails.php",
        method: "POST",
        data: data,
        beforeSend: function () {
            $("#loaderDiv").show();
        },
        success: function (response) {
            $("#loaderDiv").hide();
            $('#showDataStudent').html(response);
        }
    });
}

function backtable() {
    let level = $('#level').val() ? $('#level').val() : $('#levelForLevel').val();
    let statusfind = $('#statusfind').val() ? $('#statusfind').val() : $('#statusfindForLevel').val();
    let usernameuid = $('#usernameuid').val() ? $('#usernameuid').val() : $('#usernameuid').val();
    let statusstudent = $('#statusstudent').val() ? $('#statusstudent').val() : $('#statusstudentForLevel').val();
    let yearofterm = $('#yearofterm').val() ? $('#yearofterm').val() : $('#yearoftermForLevel').val();
    let term = $('#term').val() ? $('#term').val() : $('#termForLevel').val();
    let schoolname = $('#organizePro').val() ? $('#organizePro').val() : $('#organize').val();
    let province = $('#province').val();
    let sumofpage = localStorage.getItem('nameOfItem');
    let sortbyupdate = $("#sortbyupdate").is(":checked") ? "true" : "false";

    if (countpage == 1) {
        return false;
    }
    countpage--;
    $('#countsum').html(countpage);
    $('#countsumlevel').html(countpage);
    $('#allofpage').html(sumofpage);
    $('#allofpageschool').html(sumofpage);
    let data = {};
    data.level = level;
    data.statusfind = statusfind;
    data.statusstudent = statusstudent;
    data.yearofterm = yearofterm;
    data.term = term;
    data.page = countpage;
    data.sortbyupdate = sortbyupdate;

    if (schoolname && schoolname == '') {
        data.schoolname = schoolname;
    }

    if (province && province == '') {
        data.province = province;
    }

    console.log(data);

    $.ajax({
        url: "../showalldetails/tablestudentalldetails.php",
        method: "POST",
        data: data,
        beforeSend: function () {
            $("#loaderDiv").show();
        },
        success: function (response) {
            $("#loaderDiv").hide();
            $('#showDataStudent').html(response);
        }
    });
}

function SelectFilter() {
    countpage = 1;
    $('#countsum').html(countpage);
    $('#countsumlevel').html(countpage);
    let yearofterm = $('#yearofterm').val();
    let term = $('#term').val();
    if (yearofterm == '' && term == '') {
        alert("เลือกภาคการศึกษาและปีการศึกษา");
        return false;
    } else if (yearofterm == '') {
        alert("เลือกปีการศึกษา");
        return false;
    } else if (term == '') {
        alert("เลือกภาคการศึกษา");
        return false;
    }
    let data = {
        level: $('#level').val(),
        statusfind: $('#statusfind').val(),
        statusstudent: $('#statusstudent').val(),
        yearofterm: $('#yearofterm').val(),
        term: $('#term').val(),
        sortbyupdate:$("#sortbyupdatelevel4").is(":checked") ? true : false
        
    }
    console.log(data);

    $.ajax({
        url: "../showalldetails/tablestudentalldetails.php",
        method: "POST",
        data: data,
        beforeSend: function () {
            $("#loaderDiv").show();
        },
        success: function (response) {
            $("#loaderDiv").hide();
            $('#showDataStudent').html(response);
        }
    });

    $.ajax({
        url: "page.php",
        method: "POST",
        data: data,
        success: function (response) {
            localStorage.setItem('nameOfItem', response);
            let sumofpage = localStorage.getItem('nameOfItem');
            $('#allofpage').html(sumofpage);
            $('#allofpageschool').html(sumofpage);
            $('#page').html(response);
        }
    });
}

function SelectFilterForOrherlevel() {
    countpage = 1;
    $('#countsum').html(countpage);
    $('#countsumlevel').html(countpage);
    let province = $('#province').val();
    let schoolname = $('#organize').val() || $('#organizePro').val();
    let level = $('#levelForLevel').val();
    let statusfind = $('#statusfindForLevel').val();
    let yearofterm = $('#yearoftermForLevel').val();
    let term = $('#termForLevel').val();
    let statusstudent = $('#statusstudentForLevel').val();
    let leveluid = $('#leveluid').val();
    let sortbyupdate = $("#sortbyupdate").is(":checked") ? "true" : "false";

    if (yearofterm == '' && term == '') {
        alert("กรุณาเลือกภาคการศึกษาและปีการศึกษา");
        return false;
    } else if (yearofterm == '') {
        alert("กรุณาเลือกปีการศึกษา");
        return false;
    } else if (term == '') {
        alert("กรุณาเลือกภาคการศึกษา");
        return false;
    } else if (statusstudent == '') {
        alert("กรูณาเลือกสาเหตุ");
        return false;
    }

    let data = {};
    data.level = level;
    data.statusfind = statusfind;
    data.statusstudent = statusstudent;
    data.yearofterm = yearofterm;
    data.term = term;
    data.province = province;
    data.schoolname = schoolname;
    data.otherlevel = true;
    data.sortbyupdate = sortbyupdate;

    console.log(data);

    if (leveluid == '3') {
        $.ajax({
            url: "../../models/querynumberlevel.php",
            method: "POST",
            data: data,
            success: function (response) {
                let arrayData = response.split("&");
                $('#tracktudentfindOther').html('<p class=number>' + arrayData[1] + '</p>');
                $('#tracktudentnotfindOther').html('<p class=number>' + arrayData[2] + '</p>');
                $('#dropstudentOther').html('<p class=number>' + arrayData[3] + '</p>');
            }
        });

        $.ajax({
            url: "../showalldetails/tablestudentalldetails.php",
            method: "POST",
            data: data,
            beforeSend: function () {
                $("#loaderDiv").show();
            },
            success: function (response) {
                $("#loaderDiv").hide();
                $('#showDataStudent').html(response);
            }
        });

        $.ajax({
            url: "page.php",
            method: "POST",
            data: data,
            success: function (response) {
                localStorage.setItem('nameOfItem', response);
                let sumofpage = localStorage.getItem('nameOfItem');
                $('#allofpage').html(sumofpage);
                $('#allofpageschool').html(sumofpage);
                $('#page').html(response);
            }
        });
    } else {
        if (province == '') {
            alert("กรุณาเลือกจังหวัด");
            return false;
        }

        $.ajax({
            url: "../../models/queryfromcenter.php",
            method: "POST",
            data: data,
            success: function (response) {
                let arrayData = response.split("&");
                $('#tracktudentfindOther').html('<p class=number>' + arrayData[1] + '</p>');
                $('#tracktudentnotfindOther').html('<p class=number>' + arrayData[2] + '</p>');
                $('#dropstudentOther').html('<p class=number>' + arrayData[3] + '</p>');
            }
        });

        $.ajax({
            url: "../showalldetails/tablestudentalldetails.php",
            method: "POST",
            data: data,
            beforeSend: function () {
                $("#loaderDiv").show();
            },
            success: function (response) {
                $("#loaderDiv").hide();
                $('#showDataStudent').html(response);
            }
        });

        $.ajax({
            url: "page.php",
            method: "POST",
            data: data,
            success: function (response) {
                localStorage.setItem('nameOfItem', response);
                let sumofpage = localStorage.getItem('nameOfItem');
                $('#allofpage').html(sumofpage);
                $('#allofpageschool').html(sumofpage);
                $('#page').html(response);
            }
        });
    }
}

function showdetailsStudent(studentuid) {
    let data = {
        studentuid: studentuid
    }

    $.ajax({
        url: "../showdetailofstudent/showdetailstudent.php",
        method: "POST",
        data: data,
        success: function (response) {
            $('#showDataStudent').html(response);
        }
    });
}

function editdetailsStudent(studentuid) {
    let data = {
        studentuid: studentuid
    }

    $.ajax({
        url: "../editdetailsstudent/editdetailsstudent.php",
        method: "POST",
        data: data,
        success: function (response) {
            $('#showDataStudent').html(response);
        }
    });
}

function savedetailsStudent(studentuid) {
    var datasave = {};
    var data = new FormData();
    var filesAddess = [];
    var checkfileadd = false;
    // เช็คละติจูด ลองจิจูด
    var checkLat = false;
    var checkLong = false;
    var types=['image/jpeg', 'image/png']; //type Picture

    //ที่อยู่ตามทะเบียนบ้าน
    let houseid = $('#houseid').val();
    let house = $('#house').val();
    let villagenumber = $('#villagenumber').val();
    let soi = $('#soi').val();
    let street = $('#street').val();
    let province = $('#provinceuid').val();
    let district = $('#district').val();
    let subdistrict = $('#subdistrict').val();
    let latitude = $('#latitude').val();
    let longitude = $('#longitude').val();
    let condo = $('#condo').val();

    function checklatlong(latitude, longitude) {
        let lat = latitude.split('.');
        let long = longitude.split('.');
        if (lat) {
            if (lat[0].length > 3 || lat[1].length != 6) {
                checkLat = true;
            }
        }

        if (long) {
            if (long[0].length > 3 || long[1].length != 6) {
                checkLong = true;
            }
        }
    }

    // ข้อมูลที่อยู่ปัจุบัน
    let houseidCurent = $('#houseid_current').val();
    let houseCurent = $('#house_current').val();
    let villagenumberCurent = $('#villagenumber_current').val();
    let soiCurent = $('#soi_current').val();
    let streetCurent = $('#street_current').val();
    let provinceCurent = $('#provinceuid_current').val();
    let districtCurent = $('#district_current').val();
    let subdistrictCurent = $('#subdistrict_current').val();
    let latitudeCurent = $('#latitude_current').val();
    let longitudeCurent = $('#longitude_current').val();
    let condoCurrent = $('#condo_current').val();

    // ข้อมูลปรับสถานะภาพ
    let statusfind001 = $('#statusstudent_1').val();
    let jurisdiction = $('#jurisdiction').val();
    let schooltypeuid = $('#schooltype').val();
    let countrydetail = $('#countrydetail').val();
    let statusfind001Drop = $('#statusstudent001_2').val();
    let statusfind001Note = $('#statusstudent001_3').val();
    let statusfind002Drop = $('#statusstudent002_2').val();
    let statusfind002Note = $('#statusstudent002_3').val();
    let statusfind003Drop = $('#statusstudent003_2').val();
    let statusfind003Note = $('#statusstudent003_3').val();
    let statusfind004Drop = $('#statusstudent004_2').val();
    let statusfind004Note = $('#statusstudent004_3').val();
    let statusfind005Drop = $('#statusstudent005_2').val();
    let statusfind005Note = $('#statusstudent005_3').val();
    let statusfind006Drop = $('#statusstudent006_2').val();
    let statusfind006Note = $('#statusstudent006_3').val();
    let statusfind007Drop = $('#statusstudent007_2').val();
    let statusfind007Note = $('#statusstudent007_3').val();
    let statusfind008Drop = $('#statusstudent008_2').val();
    let statusfind008Note = $('#statusstudent008_3').val();

    // ข้อมูลรูปภาพนักเรียน
    var filePicstudent = $('#picstudent')[0].files;
    for (let i = 1; i <= index; i++) {
        let dataFile = $('#addpic_' + i)[0].files[0];
        if (dataFile == undefined) {
            checkfileadd = true;
            break;
        }
        filesAddess.push(dataFile);
    }

    // ที่อยู่ปัจจุบัน
    if (!provinceCurent || provinceCurent == '') {
        alert("กรุณาเลือกจังหวัด ที่อยู่ปัจจุบัน");
        return false;
    } else if (!districtCurent || districtCurent == '') {
        alert("กรุณาเลือกอำเภอ ที่อยู่ปัจจุบัน");
        return false;
    } else if (!subdistrictCurent || subdistrictCurent == '') {
        alert("กรุณาเลือกตำบล ที่อยู่ปัจจุบัน");
        return false;
    } else if (!houseCurent) {
        alert("กรุณากรอกเลขที่บ้าน ที่อยู่ปัจจุบัน");
        return false;
    } else if (!villagenumberCurent) {
        alert("กรุณากรอกหมู่ที่ ที่อยู่ปัจจุบัน");
        return false;
    } else if (!latitudeCurent) {
        alert("กรุณากรอกละติจูด ที่อยู่ปัจจุบัน");
        return false;
    } else if (!longitudeCurent) {
        alert("กรุณากรอกลองจิจูด ที่อยู่ปัจจุบัน");
        return false;
    }
    if (latitudeCurent != '' || longitudeCurent != '') {
        checklatlong(latitudeCurent, longitudeCurent);
        if (checkLat || checkLong) {
            alert("กรุณากรอก ละติจูด ลองจิจูด ที่อยู่ปัจจุบัน ให้ถูกต้อง \n1.หน้าจุดไม่เกิน 3 ตัว \n2.หลังจุด 6 ตัว \n ตัวอย่าง 13.766132,100.473465");
            return false;
        }
    }
    // ที่อยู่ตามทะเบียนบ้าน
    if (!province) {
        alert("กรุณาเลือกจังหวัด ที่อยู่ตามทะเบียนบ้าน");
        return false;
    } else if (!district || district == '') {
        alert("กรุณาเลือกอำเภอ ที่อยู่ตามทะเบียนบ้าน");
        return false;
    } else if (!subdistrict || subdistrict == '') {
        alert("กรุณาเลือกตำบล ที่อยู่ตามทะเบียนบ้าน");
        return false;
    } else if (!house) {
        alert("กรุณากรอกเลขที่บ้าน ที่อยู่ตามทะเบียนบ้าน");
        return false;
    } else if (!villagenumber) {
        alert("กรุณากรอกหมู่ที่ ที่อยู่ตามทะเบียนบ้าน");
        return false;
    } else if (!latitude) {
        alert("กรุณากรอกละติจูด ที่อยู่ตามทะเบียนบ้าน");
        return false;
    } else if (!longitude) {
        alert("กรุณากรอกลองจิจูด ที่อยู่ตามทะเบียนบ้าน");
        return false;
    }
    if (latitude != '' || longitude != '') {
        checklatlong(latitude, longitude);
        if (checkLat || checkLong) {
            alert("กรุณากรอก ละติจูด ลองจิจูด ที่อยู่ตามทะเบียนบ้าน ให้ถูกต้อง \n1.หน้าจุดไม่เกิน 3 ตัว \n2.หลังจุด 6 ตัว \n ตัวอย่าง 13.766132,100.473465");
            return false;
        }
    }
    // ปรับสถานะ
    if (!statusfind001) {
        alert("กรุณาเลือกสาเหตุ");
        return false;
    }
    //รูปนักเรียน
    if (filePicstudent.length <= 0) {
        alert("กรุณาใส่รูปนักเรียน");
        return false;
    }
    if (checkfileadd) {
        alert("กรุณาใส่รูปที่อยู่นักเรียน");
        return false;
    }

    datasave.houseid = houseid;
    datasave.house = house;
    datasave.villagenumber = villagenumber;
    datasave.soi = soi;
    datasave.street = street;
    datasave.province = province;
    datasave.district = district;
    datasave.subdistrict = subdistrict;
    datasave.studentuid = studentuid;
    datasave.latitude = latitude;
    datasave.longitude = longitude;
    datasave.condo = condo;

    datasave.houseidCurent = houseidCurent;
    datasave.houseCurent = houseCurent;
    datasave.villagenumberCurent = villagenumberCurent;
    datasave.soiCurent = soiCurent;
    datasave.streetCurent = streetCurent;
    datasave.provinceCurent = provinceCurent;
    datasave.districtCurent = districtCurent;
    datasave.subdistrictCurent = subdistrictCurent;
    datasave.latitudeCurent = latitudeCurent;
    datasave.longitudeCurent = longitudeCurent;
    datasave.condoCurrent = condoCurrent;

    datasave.statusfind = statusfind001;
    if (statusfind001 == '001') {
        if (jurisdiction) {
            datasave.jurisdiction = jurisdiction;
        }
        if (schooltypeuid) {
            datasave.schooltypeuid = schooltypeuid;
        }
        if (countrydetail) {
            datasave.countrydetail = countrydetail;
        }
        datasave.statusfindDrop = statusfind001Drop;
        datasave.statusfindNote = statusfind001Note;
    } else if (statusfind001 == '002') {
        datasave.statusfindDrop = statusfind002Drop;
        datasave.statusfindNote = statusfind002Note;
    } else if (statusfind001 == '003') {
        datasave.statusfindDrop = statusfind003Drop;
        datasave.statusfindNote = statusfind003Note;
    } else if (statusfind001 == '004') {
        datasave.statusfindDrop = statusfind004Drop;
        datasave.statusfindNote = statusfind004Note;
    } else if (statusfind001 == '005') {
        datasave.statusfindDrop = statusfind005Drop;
        datasave.statusfindNote = statusfind005Note;
    } else if (statusfind001 == '006') {
        datasave.statusfindDrop = statusfind006Drop;
        datasave.statusfindNote = statusfind006Note;
    } else if (statusfind001 == '007') {
        datasave.statusfindDrop = statusfind007Drop;
        datasave.statusfindNote = statusfind007Note;
    } else if (statusfind001 == '008') {
        datasave.statusfindDrop = statusfind008Drop;
        datasave.statusfindNote = statusfind008Note;
    }

    //data Picture
    data.append('studentuid', studentuid);
    if(types.includes(filePicstudent[0].type)){
        data.append('picstudent', filePicstudent[0]);
    }else{
        alert("ประเภทรูปภาพ นักเรียนไม่ถูกต้อง");
        return false;
    }
    for (let j = 0; j < filesAddess.length; j++) {
        if(types.includes(filesAddess[j].type)){
            data.append('addpic[]', filesAddess[j]);
        }else{
            alert("ประเภทรูปภาพ ที่อยู่นักเรียนไม่ถูกต้อง");
            return false;
        }
    }
    $.ajax({
        url: '../../controllers/uploadspicture.controller.php',
        type: 'POST',
        data: data,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: true,
        success: function (response) {
        },
    });

    $.ajax({
        url: "../../controllers/editdetilsstudent.controller.php",
        method: "POST",
        data: datasave,
        success: function (response) {
            if (response == 'S') {
                $('#statusupdate').html('<div class="alert alert-success" role="alert">ระบบได้บันทึกข้อมูลเรียบร้อยแล้ว</div>');

                // setTimeout(function () {
                //     window.location.replace("mainapp.php");
                // }, 1500)
            } else if (response == 'W') {
                $('#statusupdate').html('<div class="alert alert-danger" role="alert">ไม่สามารถบันทึกข้อมูลได้ กรุณากรอกข้อมูลที่บังคับกรอกให้ครบถ้วน</div>');
            }
        }
    });
}

function checkstatus() {
    let statusStep1 = $('#statusstudent_1').val();
    if (statusStep1 == '001') {
        $('#statusstudent001_2').show();
        $('#statusstudent001_3').show();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '002') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').show();
        $('#statusstudent002_3').show();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '003') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').show();
        $('#statusstudent003_3').show();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '004') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').show();
        $('#statusstudent004_3').show();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '005') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').show();
        $('#statusstudent005_3').show();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '006') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').show();
        $('#statusstudent006_3').show();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '007') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').show();
        $('#statusstudent007_3').show();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    } else if (statusStep1 == '008') {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').show();
        $('#statusstudent008_3').show();
        $('#schooltype').hide();
    } else {
        $('#statusstudent001_2').hide();
        $('#statusstudent001_3').hide();
        $('#statusstudent002_2').hide();
        $('#statusstudent002_3').hide();
        $('#statusstudent003_2').hide();
        $('#statusstudent003_3').hide();
        $('#statusstudent004_2').hide();
        $('#statusstudent004_3').hide();
        $('#statusstudent005_2').hide();
        $('#statusstudent005_3').hide();
        $('#statusstudent006_2').hide();
        $('#statusstudent006_3').hide();
        $('#statusstudent007_2').hide();
        $('#statusstudent007_3').hide();
        $('#statusstudent008_2').hide();
        $('#statusstudent008_3').hide();
        $('#schooltype').hide();
    }

    let status001new = $('#statusstudent001_3').val();
    if (status001new == "ศึกษาที่ใหม่" && statusStep1 == '001') {
        $('#jurisdiction').show();
    } else {
        $('#jurisdiction').hide();
    }

    if (status001new == "ศึกษาต่างประเทศ" && statusStep1 == '001') {
        $('#country').show();
    } else {
        $('#country').hide();
    }

}

function shJurisdiction(studentuid) {
    let jurisdictionuid = $('#jurisdiction').val();
    let data = {
        jurisdictionuid: jurisdictionuid,
    }

    $.ajax({
        url: "../editdetailsstudent/schooledit.direct.php",
        method: "POST",
        data: data,
        success: function (response) {
            $('#shJurisdiction').html(response);
        }
    });
}

function addressSel(type, typeDataAddress) {
    let data = {
        type: type
    }
    var province = $('#province').val();
    if (typeDataAddress == 'current') {
        let provinceCurent = $('#provinceuid_current').val();
        let districtCurent = $('#district_current').val();

        if (type == 'province') {
            if (provinceCurent) {
                data.provinceCurent = provinceCurent;
            }
            $.ajax({
                url: "../editdetailsstudent/addesssel.direct.php",
                method: "POST",
                data: data,
                success: function (response) {
                    $('#district_current').html(response);
                }
            });
        } else if (type == 'district') {
            if (districtCurent) {
                data.districtCurent = districtCurent;
            }
            $.ajax({
                url: "../editdetailsstudent/addesssel.direct.php",
                method: "POST",
                data: data,
                success: function (response) {
                    $('#subdistrict_current').html(response);
                }
            });
        }
    } else if (typeDataAddress == 'home') {
        let provinceuid = $('#provinceuid').val();
        let district = $('#district').val();

        if (type == 'province') {
            if (provinceuid) {
                data.provinceuid = provinceuid;
            }
            $.ajax({
                url: "../editdetailsstudent/addesssel.direct.php",
                method: "POST",
                data: data,
                success: function (response) {
                    $('#district').html(response);
                }
            });
        } else if (type == 'district') {
            if (district) {
                data.district = district;
            }
            $.ajax({
                url: "../editdetailsstudent/addesssel.direct.php",
                method: "POST",
                data: data,
                success: function (response) {
                    $('#subdistrict').html(response);
                }
            });
        }
    }else if(typeDataAddress == 'mainsh'){
        if (type == 'shcool') {
            if (province) {
                data.provinceuid = province;
            }
            $.ajax({
                url: "../editdetailsstudent/addesssel.direct.php",
                method: "POST",
                data: data,
                success: function (response) {
                    $('#organize').html(response);
                }
            });
        }
    }
}

function exportexcel() {
    let data = {};
    data.level = $('#level').val();
    data.statusfind = $('#statusfind').val();
    data.statusstudent = $('#statusstudent').val();
    data.yearofterm = $('#yearofterm').val();
    data.term = $('#term').val();
    console.log(data);
    $.ajax({
        url: "../../controllers/exportexcel.controller.php",
        method: "POST",
        data: data,
        success: function (response) {
        }
    });

}

function addData() {
    if (index > 4) {
        alert("ไม่สามารถเพิ่มข้อมูลได้");
        return;
    }
    index++;
    $('#dynamic_field').append("<tr id='address_row" + index + "'><td><input name='addpic[]' id='addpic_" + index + "' type='file' class='form-control' placeholder='กรุณาใส่ลองจิจูด' title='ภาษาอังกฤษหรือตัวเลขเท่านั้น' maxlength='11' /></td><td><input type='button' value='-' id='" + index + "' onclick='removeData(" + index + ")'></button></td></tr>");
}

function removeData(data) {
    index--;
    $('#address_row' + data + '').remove();
}