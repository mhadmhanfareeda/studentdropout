<?php
include "../../models/user.php";
$studentuid = $_POST["studentuid"];
$obj = new User;
$data = $obj->queryUserdetails($studentuid);
$dataStudent = json_decode($data);
?>

<?php
if($dataStudent->{'statusDrop_id'} != '009'){?>
    <h5 align="center"><img style="text-align: center;width:50px;" src="../../public/fileupload/student/<?=$studentuid?>.png"/></h5>
<?php
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-xl-12">

            <form name="register" action="register_db.php" method="POST" id="register" class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-2" align="right"> รหัสประจำตัวประชาชน : </div>
                    <div class="col-sm-6" align="left">
                        <input disabled name="Admin_username" type="text" value="<?= $dataStudent->{'personuid'} ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2" align="right"> รหัสนักเรียน : </div>
                    <div class="col-sm-6" align="left">
                        <input disabled name="Admin_username" type="text" value="<?= $studentuid ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2" align="right"> ชื่อ-นามสกุล : </div>
                    <div class="col-sm-6" align="left">
                        <input disabled name="Admin_username" type="text" value="<?= $dataStudent->{'firstname'} ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2" align="right"> ที่อยู่ปัจจุบัน : </div>
                    <div class="col-sm-6" align="left">
                        <input disabled name="Admin_username" type="text" value="<?= "จ.".$dataStudent->{'provinceCurrent'}." อ.".$dataStudent->{'districtCurrent'}." ต.".$dataStudent->{'subdistrictCurrent'} ?>" class="form-control" id="Admin_username" placeholder="username" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2" align="right"> สถานะการติดตาม : </div>
                    <div class="col-sm-6" align="left">
                        <p <?php if ($dataStudent->{'status'} == 'ติดตามแล้ว ไม่พบตัว') {
                                echo "style='color:red'; ";
                            } else if ($dataStudent->{'status'} == 'ติดตามแล้ว พบตัว') {
                                echo "style='color:green'; ";
                            } ?>><?= $dataStudent->{'status'} ?></p>
                    </div>
                </div>
                <?php
                if ($dataStudent->{'status'} != 'หลุดจากระบบ') {
                ?>
                    <div class="form-group">
                        <div class="col-sm-2" align="right"></div>
                        <div class="col-sm-10" align="left">
                            <input disabled name="statusDrop" type="text" value="<?= $dataStudent->{'statusDrop'} ?>" class="form-control" id="statusDrop"  minlength="2" />
                        </div>
                    </div>
                <?php
                }
                ?>
                <div class="col-sm-3"> </div>
                <div class="col-sm-7">
                    <button type="button" class="btn btn-primary" id="btn" onclick="editdetailsStudent(<?= $studentuid ?>)"> แก้ไขข้อมูล </button>
                </div>
        </div>
        </form>
    </div>
</div>
</div>