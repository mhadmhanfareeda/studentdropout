<?php
include "../../configs/db.php";
$con = OpenCon();

class User
{
    public function queryUserdetails($studentuid)
    {
        $notFind = array('004', '006');
        $find = array('001', '002', '003', '005', '007', '008');
        $status = '';
        $statusdetails = "";

        $query = "SELECT * FROM totalstudent2565_1 WHERE student_id = '$studentuid'";
        $result =  $GLOBALS['con']->query($query);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $studentname = $row["firstname"] . " " . $row["lastname"];
            $personuid = $row["person_id"];
            $districtuid = $row["district_id"];
            $provinceuid = $row["province_id"];
            $subdistrictuid = $row["subdistrict_id"];

            $districtuidCurrent = $row["present_district_id"]; //อำเภอตามที่อยู่ปัจจุบัน
            $provinceuidCurrent = $row["present_province_id"]; //จังหวัดตามที่อยู่ปัจจุบัน
            $subdistrictuidCurrent = $row["present_subdistrict_id"]; //ตำบลตามที่อยู่ปัจจุบัน

            $statusnote = $row["status_note"];
            $studentstausuid = $row["studentstaus_id"];
            $note = $row["note"];
            $schooltypeuid = @$row["schooltype_id"]; //schooltype_id โรงเรียนตามสังกัด
            $jurisdictionuid = @$row["jurisdiction_id"]; // jurisdiction_id สังกัด
            $street = $row["street"];
            $soi = $row["soi"];
            $villagenumber = $row["villagenumber"];
            $house = $row["house"];
            $houseid = $row["house_id"];
            $latitude = $row["latitude"];
            $longitude = $row["longitude"];
            $updated = $row["updated"]; //วันที่อัปเดตข้อมูล
            $created = $row["created"]; //วันที่เพิ่มข้อมูล
            $academicyear = $row["academicyear"]; //ปีการศึกษา
            $semester = $row["semester"]; //ภาคการศึกษา
            $organize_id = $row["organize_id"]; //รหัสหน่วยงานสำนักงาน กศน.
            $school_id = $row["school_id"]; //รหัสสถานศึกษา/หน่วยงาน
            $prefix_id = $row["prefix_id"]; //รหัสคำนำหน้าชื่อ
            $prefixName = '';
            $gender_id = $row["gender_id"]; //รหัสเพศ
            $birthdate = $row["birthdate"]; //วันเดือนปีเกิด
            $nationalityuid = $row["nationality_id"]; //รหัสสัญชาติ
            $disabilityuid = $row["disability_id"]; //รหัสความพิการ
            $disadvantageeducationuid = $row["disadvantageeducation_id"]; //รหัสความด้อยโอกาส
            $passportnumber = $row["passportnumber"]; //เลขที่หนังสือเดินทาง
            $house_id = $row["house_id"]; //รหัสประจำบ้าน
            $middlename = $row["middlename"]; //ชื่อกลาง
            $province = '';  // ดึงข้อมูลจังหวัด
            $district = '';  // ดึงข้อมูลอำเภอ
            $subdistrict = '';  // ดึงข้อมูลตำบล
            $nationality_id = ''; // ดึงข้อมูลสัญชาติ
            $nationality_name = ''; // ดึงข้อมูลสัญชาติ

            $condo = $row["other_addres_home"]; //ชื่อคอนโด ที่อยู่ตามทะเบียนบ้าน
            $condoCurrent = $row["other_addres_present"]; //ชื่อกลาง ที่อยู่ปัจจุบัน

            // Status
            if (in_array($studentstausuid, $notFind)) {
                $status = "ติดตามแล้ว ไม่พบตัว";
            } else if (in_array($studentstausuid, $find)) {
                $status = "ติดตามแล้ว พบตัว";
            } else {
                $status = "หลุดจากระบบ";
            }

            // ดึงชื่อจังหวัดอำเภอตำบลเพื่อแสดงในหน้าแก้ไขข้อมูล
            // ดึงข้อมูลอำเภอ (ตามที่อยู่ตามทะเบียนบ้าน)
            $queryDistrict = "SELECT district_name FROM m_district WHERE district_id = '$districtuid'";
            $resultDis =  $GLOBALS['con']->query($queryDistrict);
            if ($resultDis->num_rows > 0) {
                $rowDis = $resultDis->fetch_assoc();
            }
            // ดึงข้อมูลจังหวัด (ตามที่อยู่ตามทะเบียนบ้าน)
            $queryProvince = "SELECT province_name FROM m_province WHERE province_id = '$provinceuid'";
            $resultPro =  $GLOBALS['con']->query($queryProvince);
            if ($resultPro->num_rows > 0) {
                $rowPro = $resultPro->fetch_assoc();
            }
            // ดึงข้อมูลตำบล(ตามที่อยู่ตามทะเบียนบ้าน)
            $querySub = "SELECT subdistrict_name FROM m_subdistrict WHERE subdistrict_id = '$subdistrictuid'";
            $resultSub =  $GLOBALS['con']->query($querySub);
            if ($resultSub->num_rows > 0) {
                $rowSub = $resultSub->fetch_assoc();
            }
            $province = $rowPro["province_name"];
            $district = $rowDis["district_name"];
            $subdistrict = $rowSub["subdistrict_name"];
            //-----------------------------------------------------------------------------

            // ดึงชื่อจังหวัดอำเภอตำบลเพื่อแสดงในหน้าแก้ไขข้อมูล
            // ดึงข้อมูลอำเภอ (ตามที่อยู่ปัจจุบัน)
            $queryDisCur = "SELECT district_name FROM m_district WHERE district_id = '" . $row['present_district_id'] . "'";
            $resultDisCur =  $GLOBALS['con']->query($queryDisCur);
            if ($resultDisCur->num_rows > 0) {
                $rowDisCur = $resultDisCur->fetch_assoc();
            }
            // ดึงข้อมูลจังหวัด (ตามที่อยู่ปัจจุบัน)
            $queryProCur = "SELECT province_name FROM m_province WHERE province_id = '" . $row['present_province_id'] . "'";
            $resultProCur =  $GLOBALS['con']->query($queryProCur);
            if ($resultProCur->num_rows > 0) {
                $rowProCur = $resultProCur->fetch_assoc();
            }
            // ดึงข้อมูลตำบล (ตามที่อยู่ปัจจุบัน)
            $querySubCur = "SELECT subdistrict_name FROM m_subdistrict WHERE subdistrict_id = '" . $row['present_subdistrict_id'] . "'";
            $resultSubCur =  $GLOBALS['con']->query($querySubCur);
            if ($resultSubCur->num_rows > 0) {
                $rowSubCur = $resultSubCur->fetch_assoc();
            }

            $provinceCurrent = @$rowProCur["province_name"] ? @$rowProCur["province_name"] : 'ไม่ได้ระบุ';
            $districtCurrent = @$rowDisCur["district_name"] ? @$rowDisCur["district_name"] : 'ไม่ได้ระบุ';
            $subdistrictCurrent = @$rowSubCur["subdistrict_name"] ? @$rowSubCur["subdistrict_name"] : 'ไม่ได้ระบุ';
            //-----------------------------------------------------------------------------

            // ดึงข้อมูลสถานะ เพื่อแสดงในหน้ารายละเอียด
            $queryStatusdrop = "SELECT status_id, status_name FROM m_status_dropout WHERE status_id = '$studentstausuid'";
            $resultStatusdrop =  $GLOBALS['con']->query($queryStatusdrop);
            if ($resultStatusdrop->num_rows > 0) {
                $rowStatusdrop = $resultStatusdrop->fetch_assoc();
            }
            if ($rowStatusdrop["status_name"] && $rowStatusdrop["status_id"]) {
                $statusdetails = $rowStatusdrop["status_name"];
                $statusdetails_id = $rowStatusdrop["status_id"];
            }
            if ($statusnote) {
                $statusdetails .= ", " . $statusnote;
            }
            if ($note) {
                $statusdetails .= ", " . $note;
            }
            if ($jurisdictionuid) {
                $queryJuris = "SELECT jurisdiction_name FROM m_jurisdiction WHERE jurisdiction_id = '$jurisdictionuid'";
                $resultJuris =  $GLOBALS['con']->query($queryJuris);
                if ($resultJuris->num_rows > 0) {
                    $rowJuris = $resultJuris->fetch_assoc();
                }
                $statusdetails .= ", " . $rowJuris["jurisdiction_name"];
            }
            if ($schooltypeuid) {
                $queryschooltype = "SELECT schooltype_name FROM m_schooltype WHERE schooltype_id = '$schooltypeuid'";
                $resultschooltype =  $GLOBALS['con']->query($queryschooltype);
                if ($resultschooltype->num_rows > 0) {
                    $rowschooltype = $resultschooltype->fetch_assoc();
                }
                $statusdetails .= ", " . $rowschooltype["schooltype_name"];
            }
            //-----------------------------------------------------------------------------

            // ดึงข้อมูลสัญชาติ
            $queryNationality = "SELECT nationality_id, nationality_name FROM m_nationality WHERE nationality_id = '$nationalityuid'";
            $resultNationality =  $GLOBALS['con']->query($queryNationality);
            if ($resultNationality->num_rows > 0) {
                $rowNationality = $resultNationality->fetch_assoc();
            }
            $nationality_id = $rowNationality["nationality_id"];
            $nationality_name = $rowNationality["nationality_name"];
            //-----------------------------------------------------------------------------

            // ดึงข้อมูลความพิการ
            $disability_id = '';
            $disability_name = '';
            $queryDisability = "SELECT disability_id, disability_name FROM m_disability WHERE disability_id = '$disabilityuid'";
            $resultDisability =  $GLOBALS['con']->query($queryDisability);
            if ($resultDisability->num_rows > 0) {
                $rowDisability = $resultDisability->fetch_assoc();
            }
            $disability_id = $rowDisability["disability_id"];
            $disability_name = $rowDisability["disability_name"];
            //-----------------------------------------------------------------------------

            // ดึงข้อมูลความด้อยโอกาส
            $disadvantageeducation_id = '';
            $disadvantageeducation_name = '';
            $queryDisadvantageeducation = "SELECT disadvantageeducation_id, disadvantageeducation_name FROM m_disadvantageeducation WHERE disadvantageeducation_id = '$disadvantageeducationuid'";
            $resultDisadvantageeducation =  $GLOBALS['con']->query($queryDisadvantageeducation);
            if ($resultDisadvantageeducation->num_rows > 0) {
                $rowDisadvantageeducation = $resultDisadvantageeducation->fetch_assoc();
            }
            $disadvantageeducation_id = $rowDisadvantageeducation["disadvantageeducation_id"];
            $disadvantageeducation_name = $rowDisadvantageeducation["disadvantageeducation_name"];
            //-----------------------------------------------------------------------------

            // ดึงคำนำหน้าชื่อ
            $queryPrefix = "SELECT prefix_name FROM m_prefix WHERE prefix_id = '$prefix_id'";
            $resultPrefix =  $GLOBALS['con']->query($queryPrefix);
            if ($resultPrefix->num_rows > 0) {
                $rowPrefix = $resultPrefix->fetch_assoc();
                $prefixName = $rowPrefix["prefix_name"];
            }
            //-----------------------------------------------------------------------------
        }

        $data = array(
            //ข้อมูลบุคคล
            "firstname" => $studentname,
            "personuid" => $personuid,
            "updated" => $updated,
            "created" => $created,
            "academicyear" => $academicyear,
            "semester" => $semester,
            "organize_id" => $organize_id,
            "school_id" => $school_id,
            "gender_id" => $gender_id,
            "birthdate" => $birthdate,
            "nationality_id" => $nationality_id,
            "nationality_name" => $nationality_name,
            "disability_id" => $disability_id,
            "disability_name" => $disability_name,
            "disadvantageeducation_id" => $disadvantageeducation_id,
            "disadvantageeducation_name" => $disadvantageeducation_name,
            "house_id" => $house_id,
            "passportnumber" => $passportnumber,
            "middlename" => $middlename,

            //ข้อมูลที่อยู่ตามทะเบียนบ้าน
            "province" => $province,
            "district_id" => $districtuid,
            "district" => $district,
            "subdistrict_id" => $subdistrictuid,
            "subdistrict" => $subdistrict,
            "street" => $street,
            "soi" => $soi,
            "house" => $house,
            "houseid" => $houseid,
            "villagenumber" => $villagenumber,
            "latitude" => $latitude,
            "longitude" => $longitude,
            "condo"=>$condo,

            //ข้อมูลที่อยู่ปัจุบัน
            "provinceuidCurrent"=>$provinceuidCurrent,
            "provinceCurrent"=>$provinceCurrent,
            "districtuidCurrent"=>$districtuidCurrent,
            "districtCurrent"=>$districtCurrent,
            "subdistrictuidCurrent"=>$subdistrictuidCurrent,
            "subdistrictCurrent"=>$subdistrictCurrent,
            "condoCurrent"=>$condoCurrent,


            "statusDrop_id" => $statusdetails_id,
            "statusDrop" => $statusdetails,
            "status" => $status,

            "prefix_id" => $prefix_id, //คำนำหน้าชื่อ
            "prefixName" => $prefixName,
            "statusnote" => $statusnote
        );

        $data = json_encode($data);
        return $data;
    }
}
