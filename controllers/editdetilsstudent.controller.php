<?php
session_start();
ob_start();
include "../configs/db.php";
$con = OpenCon();
$concat = '';
$studentuid = @$_POST['studentuid'];
$statusfind = @$_POST['statusfind'];
$statusfindDrop = @$_POST['statusfindDrop'];
$statusfindNote = @$_POST['statusfindNote'];
$jurisdiction = @$_POST['jurisdiction'];
$schooltypeuid = @$_POST['schooltypeuid'];
$countrydetail = @$_POST['countrydetail'];
$latitude = @$_POST['latitude'];
$longitude = @$_POST['longitude'];
$houseid = @$_POST['houseid'];
$house = @$_POST['house'];
$home_other_address =@$_POST['home_other_address'];
$villagenumber = @$_POST['villagenumber'];
$soi = @$_POST['soi'];
$street = @$_POST['street'];
$province = @$_POST['province'];
$district = @$_POST['district'];
$subdistrict = @$_POST['subdistrict'];
$condo = @$_POST['condo'];

$houseidCurent = @$_POST['houseidCurent'];
$houseCurent = @$_POST['houseCurent'];
$villagenumberCurent = @$_POST['villagenumberCurent'];
$soiCurent = @$_POST['soiCurent'];
$streetCurent = @$_POST['streetCurent'];
$provinceCurent = @$_POST['provinceCurent'];
$districtCurent = @$_POST['districtCurent'];
$subdistrictCurent = @$_POST['subdistrictCurent'];
$latitudeCurent = @$_POST['latitudeCurent'];
$longitudeCurent = @$_POST['longitudeCurent'];
$condoCurrent = @$_POST['condoCurrent'];

$date2 = new DateTime();
$date2->setTimezone(new DateTimeZone('Asia/Bangkok'));
$editDate = $date2->format('Y-m-d H:i:s');

//ข้อมูลที่อยู่ตามทะเบียนบ้าน
if($jurisdiction && $schooltypeuid){
    $concat=", jurisdiction_id = '{$jurisdiction}', schooltype_id = '{$schooltypeuid}'";
}

if($countrydetail){
    $concat.=", note = '{$countrydetail}'";
}

if($houseid && $houseid!=''){
    $concat.=", house_id = '{$houseid}'";
}

if($house && $house!=''){
    $concat.=", house = '{$house}'";
}

if($villagenumber && $villagenumber!=''){
    $concat.=", villagenumber = '{$villagenumber}'";
}

if($soi && $soi!=''){
    $concat.=", soi = '{$soi}'";
}

if($street && $street!=''){
    $concat.=", street = '{$street}'";
}

if($province && $province!=''){
    $concat.=", province_id = '{$province}'";
}

if($district && $district!=''){
    $concat.=", district_id = '{$district}'";
}

if($subdistrict && $subdistrict!=''){
    $concat.=", subdistrict_id = '{$subdistrict}'";
}

if($latitude && $latitude!=''){
    $concat.=", latitude = '{$latitude}'";
}

if($longitude && $longitude!=''){
    $concat.=", longitude = '{$longitude}'";
}

if($condo && $condo!=''){
    $concat.=", other_addres_home = '{$condo}'";
}
//----------------------------------------------------------

//ข้อมูลที่อยู่ปัจุบัน
if($houseidCurent && $houseidCurent!=''){
    $concat.=", present_house_id = '{$houseidCurent}'";
}
if($houseCurent && $houseCurent!=''){
    $concat.=", present_house = '{$houseCurent}'";
}
if($villagenumberCurent && $villagenumberCurent!=''){
    $concat.=", present_villagenumber = '{$villagenumberCurent}'";
}
if($soiCurent && $soiCurent!=''){
    $concat.=", present_soi = '{$soiCurent}'";
}
if($streetCurent && $streetCurent!=''){
    $concat.=", present_street = '{$streetCurent}'";
}
if($provinceCurent && $provinceCurent!=''){
    $concat.=", present_province_id = '{$provinceCurent}'";
}
if($districtCurent && $districtCurent!=''){
    $concat.=", present_district_id = '{$districtCurent}'";
}
if($subdistrictCurent && $subdistrictCurent!=''){
    $concat.=", present_subdistrict_id = '{$subdistrictCurent}'";
}
if($latitudeCurent && $latitudeCurent!=''){
    $concat.=", present_latitude = '{$latitudeCurent}'";
}
if($longitudeCurent && $longitudeCurent!=''){
    $concat.=", present_longitude = '{$longitudeCurent}'";
}
if($condoCurrent && $condoCurrent!=''){
    $concat.=", other_addres_present = '{$condoCurrent}'";
}
//----------------------------------------------------------

//Status
if($statusfindDrop){
    $concat.=", status_drop = '{$statusfindDrop}'";
}
if($statusfindNote){
    $concat.=", status_note = '{$statusfindNote}'";
}
//----------------------------------------------------------

$updatepassnew = "UPDATE totalstudent2565_1 SET studentstaus_id = '{$statusfind}', updated='{$editDate}' $concat WHERE student_id = '{$studentuid}'";
$result = $con->query($updatepassnew);

if ($result) {
    echo "S";
} else {
    echo "W";
}
