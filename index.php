<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <?php include "configs/resoures.php"; ?>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <?php include "configs/resoures.php"; ?>
</head>
<style>
    body {
        background-image: url("");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
    }
</style>
<div align="center"><img src="../studentdropout/public/logonfe.png" height="200"></div>
<div class="header">
    <h3><B>เข้าสู่ระบบ</B></h3>
</div>
<form class="w3-container w3-card-4 w3-light-grey">
    <br><label>Username</label>
    <input class="w3-input w3-border w3-round" name="username" id="username" type="text">
    </p>
    <p>
        <label>Password</label>
        <input class="w3-input w3-border w3-round-large" name="password" id="password" type="text">
    </p>
    <div>
        <input type="button" name="loginuser" id="loginuser" class="btn" value="LOGIN" onclick="checkLogin()" />
    </div>
    <br>
</form>
<div id="statuslogin"></div>
<br><br>
<div align="center">
    <h4><b> ระบบรายงานสารสนเทศของสำนักงาน กศน.</b></h4>
    <h5>สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย สำนักงานปลัดกระทรวงศึกษาธิการ </h5>
</div>

<script>
    function checkLogin() {
        let data = {
            username: $('#username').val(),
            password: $('#password').val(),
            login_user: $('#loginuser').val(),
        }

        $.ajax({
            url: "chacklogin.php",
            method: "POST",
            data: data,
            success: function(response) {
                if (response == 'S') {
                    $('#statuslogin').html('<div class="alert alert-success" role="alert">Your are now logged in</div>');

                    setTimeout(function() {
                        window.location.replace("views/main/mainapp.php");
                    }, 1000)
                } else if (response == 'W') {
                    $('#statuslogin').html('<div class="alert alert-danger" role="alert">Wrong Username or Password!</div>');
                } else if (response == 'LW') {
                    $('#statuslogin').html('<div class="alert alert-danger" role="alert">NO ACCESS SYSTEMS!</div>');
                } else {
                    $('#statuslogin').html('<div class="alert alert-warning" role="alert">Username & Password is required</div>');
                }
            }
        });
    }
</script>